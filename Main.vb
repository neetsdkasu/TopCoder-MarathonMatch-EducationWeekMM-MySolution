Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Try
            Dim N As Integer = CInt(Console.ReadLine())
			Dim K As Integer = CInt(Console.ReadLine())
            Dim constraints(K - 1) As String
            For i As Integer = 0 To UBound(constraints)
                constraints(i) = Console.ReadLine()
            Next i
            
            Dim cp As New ConstrainedPermutation()
            Dim ret() As Integer = cp.permute(N, constraints)

            Console.WriteLine(ret.Length)
            For Each r As Integer In ret
                Console.WriteLine(r)
            Next r
            Console.Out.Flush()
            
        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module