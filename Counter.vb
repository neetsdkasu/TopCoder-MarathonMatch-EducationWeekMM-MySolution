Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System

Public Module Counter

    Public Sub Main()
        Try
			Dim args() As String = Environment.GetCommandLineArgs()
			Dim echo As Boolean = args.Length > 1
			Dim n as Integer = 0
			Dim sum As Double = 0.0
			Do
				Dim s As String = Console.ReadLine()
				
				If s Is Nothing Then Exit Do
				
				Dim ss() As String = s.Split(" "c)
				If ss.Length = 3 AndAlso ss(1) = "=" Then				
					n += 1
					sum += CDbl(ss(2))
					
					If echo Then Console.Write("{0,4}: ", n)
				End If

				If echo Then Console.WriteLine(s)
				
			Loop
			Console.WriteLine("n:   {0}", n)
			Console.WriteLine("sum: {0}", sum)
            Console.WriteLine("avg: {0}", sum / CDbl(n))
			
        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module