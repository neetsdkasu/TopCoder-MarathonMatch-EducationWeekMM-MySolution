Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.IO
Imports System.Collections.Generic

Public Module DiffScore

    Public Sub Main()
        Try
			Dim args() As String = Environment.GetCommandLineArgs()
			
			If args.Length < 2 OrElse 3 < args.Length Then
				Console.Error.WriteLine("usage")
				Console.Error.WriteLine("  args: file1 file2")
				Console.Error.WriteLine(" or")
				Console.Error.WriteLine("  args: file1   (with Stdin)")
				Exit Sub
			End If
			
			Dim filemode As Boolean = args.Length = 3
			
			Dim score1 As New List(Of Double)()
			
			Using reader As StreamReader = File.OpenText(args(1))
				Do
					Dim s As String = reader.ReadLine()
					If s Is Nothing Then Exit Do
					
					Dim ss() As String = s.Split(" "c)
					
					If ss.Length <> 3 Then Continue Do
					If ss(1) <> "=" Then Continue Do
					
					score1.Add(CDbl(ss(2)))
				Loop
			End Using
			
			Dim score2 As New List(Of Double)()
			
			If filemode Then
				Using reader As StreamReader = File.OpenText(args(2))
					Do
						Dim s As String = reader.ReadLine()
						If s Is Nothing Then Exit Do
						
						Dim ss() As String = s.Split(" "c)
						
						If ss.Length <> 3 Then Continue Do
						If ss(1) <> "=" Then Continue Do
						
						score2.Add(CDbl(ss(2)))
					Loop
				End Using
			Else
				Using writer As StreamWriter = File.CreateText("_stdin.txt")
					Do
						Dim s As String = Console.ReadLine()
						If s Is Nothing Then Exit Do
						writer.WriteLine(s)
						
						Dim ss() As String = s.Split(" "c)
						
						If ss.Length <> 3 Then Continue Do
						If ss(1) <> "=" Then Continue Do
						
						score2.Add(CDbl(ss(2)))
					Loop
				End Using
			End If
			
			Dim n As Integer = Math.Min(score1.Count, score2.Count)

			If n = 0 Then
				Console.Error.WriteLine("no data")
				Console.Error.WriteLine("file1 data size: {0}", score1.Count)
				Console.Error.WriteLine("file2 data size: {0}", score2.Count)
				Exit Sub
			End If
			
			Dim file1 As String = Path.GetFileNameWithoutExtension(args(1))
			Dim file2 As String = If(filemode, Path.GetFileNameWithoutExtension(args(2)), "stdin")
			If file1.Length > 18 Then file1 = file1.Substring(0, 18)
			If file2.Length > 18 Then file2 = file2.Substring(0, 18)
			
			Console.WriteLine("score change summary from {0} to {1}", file1, file2)
			Console.WriteLine("       {0,-18}  {1,-18}  {2,-18}  {3,-8}", _
				file1, file2, "diff", "rate")
			
			Dim plus As Integer = 0
			Dim minus As Integer = 0
			Dim sum1 As Double = 0.0
			Dim sum2 As Double = 0.0
			Dim per As Double = Double.NaN
			For i As Integer = 0 To n - 1
				Dim sc1 As Double = score1(i)
				Dim sc2 As Double = score2(i)
				Select Case Math.Sign(sc2 - sc1)
				Case -1
					minus += 1
				Case 1
					plus += 1
				End Select
				If Math.Sign(sc1) <> 0 Then
					per = (sc2-sc1) / sc1
				Else
					per = Double.NaN
				End If
				Console.WriteLine( _
					"{3,4:D}:  {0,18:F08}  {1,18:F08}  {2,18:F08}  {4,8:F04}", _
					sc1, sc2, sc2-sc1, i+1, per)
				sum1 += sc1
				sum2 += sc2
				If (i + 1) Mod 5 = 0 AndAlso (i + 1) <> n Then
					Console.WriteLine("-------------------------------------------------------------------")
				End If
			Next i
			
			If Math.Sign(sum1) <> 0 Then
				per = (sum2-sum1) / sum1
			Else
				per = Double.NaN
			End If
			
			Console.WriteLine("-------------------------------------------------------------------")
			Console.WriteLine(" sum:  {0,18:F08}  {1,18:F08}  {2,18:F08}  {3,8:F04}", _
				sum1, sum2, sum2-sum1, per)

			Dim avg1 As Double = sum1 / CDbl(n)
			Dim avg2 As Double = sum2 / CDbl(n)
			
			If Math.Sign(avg1) <> 0 Then
				per = (avg2-avg1) / avg1
			Else
				per = Double.NaN
			End If
			
			Console.WriteLine("-------------------------------------------------------------------")
			Console.WriteLine(" avg:  {0,18:F08}  {1,18:F08}  {2,18:F08}  {3,8:F04}", _
				avg1, avg2, avg2-avg1, per)
			
			Console.WriteLine("-------------------------------------------------------------------")
			Console.WriteLine("changes +{0}/-{1}  (+{2:F03}/-{3:F03})", _
				plus, minus, CDbl(plus)/CDbl(n), CDbl(minus)/CDbl(n))
			
        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module