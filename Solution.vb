Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)

Public Class ConstrainedPermutation
    
	Public Function permute(N As Integer, constraints() As String) As Integer()
		Dim t0 As Integer = Environment.TickCount + 9800
		Dim t1 As Integer
		Dim XSFT As ULong = 88172645463325252UL
		Dim UMAX As Double = CDbl(ULong.MaxValue)
		
		Dim flag(N - 1, N - 1) As Boolean
		Dim graphFs(N - 1) As List(Of Integer)
		Dim graphTs(N - 1) As List(Of Integer)
		For i As Integer = 0 To N - 1
			graphFs(i) = New List(Of Integer)()
			graphTs(i) = New List(Of Integer)()
		Next i
		
		Dim K As Integer = constraints.Length
		Dim pairs(K - 1) As Tp
		For i As Integer = 0 To K - 1
			Dim ss() As String = constraints(i).Split(" "c)
			Dim t As New Tp(CInt(ss(0)), CInt(ss(1)))
			pairs(i) = t
			graphFs(t.Item2).Add(t.Item1)
			graphTs(t.Item1).Add(t.Item2)
			flag(t.Item1, t.Item2) = True
		Next i
		
		Dim useGreedy As Boolean = _
			(N <= 300 AndAlso K < 20000) _
			OrElse (N <= 500 AndAlso K < 10000) _
			OrElse (K < 5000)
		
		Dim ret(N - 1) As Integer
        Dim tmp(N - 1) As Integer
		For i As Integer = 0 To N - 1
			ret(i) = i
			tmp(i) = i
		Next i
		
		Dim score As Integer = 0
		For Each p As Tp In pairs
			If tmp(p.Item1) < tmp(p.Item2) Then score += 1
		Next p
		Dim maxScore As Integer = score
        Dim baseScore As Integer = score
		
		Const Alpha As Double = 0.00000000005
		Const MaxTime As Double = 10000000.0
		
		Dim UN As ULong = CULng(N)
		
		Do
			
			If useGreedy Then
				
				Dim tmpScore As Integer = score
			
				For i As Integer = 0 To N - 2
					
					t1 = Environment.TickCount
					If t0 < t1 Then Exit Do
					
					For j As Integer = i + 1 To N - 1
						Dim tmpi As Integer = ret(i)
						Dim tmpj As Integer = ret(j)
						
						Dim diff As Integer = 0
						For Each f As Integer In graphFs(i)
							If ret(f) < tmpi Then diff -= 1
							If ret(f) < tmpj Then diff += 1
						Next f
						For Each t As Integer In graphTs(i)
							If tmpi < ret(t) Then diff -= 1
							If tmpj < ret(t) Then diff += 1
						Next t
						For Each f As Integer In graphFs(j)
							If ret(f) < tmpj Then diff -= 1
							If ret(f) < tmpi Then diff += 1
						Next f
						For Each t As Integer In graphTs(j)
							If tmpj < ret(t) Then diff -= 1
							If tmpi < ret(t) Then diff += 1
						Next t
						If flag(i, j) Then
							If tmpi < tmpj Then diff += 1
							If tmpj < tmpi Then diff += 1
						End If
						If flag(j, i) Then
							If tmpj < tmpi Then diff += 1
							If tmpi < tmpj Then diff += 1
						End if
						
						If diff < 0 Then Continue For
						
						ret(j) = tmpi
						ret(i) = tmpj
						tmpScore += diff
						
					Next j
				Next i
				
				If tmpScore > maxScore Then
					maxScore = tmpScore
				End If
				
			End If
			
			Dim t2 As Integer = Environment.TickCount + 3300
			If t2 > t0 Then t2 = t0
			
			Dim c As Integer = 0
			
			Do
				c += 1
				
				t1 = Environment.TickCount
				If t2 < t1 Then Exit Do
				
				XSFT = XSFT Xor (XSFT << 13)
				XSFT = XSFT Xor (XSFT >> 17)
				XSFT = XSFT Xor (XSFT << 5)
				Dim ui As ULong = XSFT Mod (UN - 1UL)
				Dim i As Integer = CInt(ui)
				XSFT = XSFT Xor (XSFT << 13)
				XSFT = XSFT Xor (XSFT >> 17)
				XSFT = XSFT Xor (XSFT << 5)
				Dim j As Integer = CInt(XSFT Mod (UN - (ui + 1UL))) + (i + 1)
				
				
				Dim tmpi As Integer = tmp(i)
				Dim tmpj As Integer = tmp(j)
				
				Dim diff As Integer = 0
				For Each f As Integer In graphFs(i)
					If tmp(f) < tmpi Then diff -= 1
					If tmp(f) < tmpj Then diff += 1
				Next f
				For Each t As Integer In graphTs(i)
					If tmpi < tmp(t) Then diff -= 1
					If tmpj < tmp(t) Then diff += 1
				Next t
				For Each f As Integer In graphFs(j)
					If tmp(f) < tmpj Then diff -= 1
					If tmp(f) < tmpi Then diff += 1
				Next f
				For Each t As Integer In graphTs(j)
					If tmpj < tmp(t) Then diff -= 1
					If tmpi < tmp(t) Then diff += 1
				Next t
				If flag(i, j) Then
					If tmpi < tmpj Then diff += 1
					If tmpj < tmpi Then diff += 1
				End If
				If flag(j, i) Then
					If tmpj < tmpi Then diff += 1
					If tmpi < tmpj Then diff += 1
				End if
				
				Dim tmpScore As Integer = score + diff

				If tmpScore > maxScore Then
					maxScore = tmpScore
					baseScore = tmpScore
					score = tmpScore
					tmp(i) = tmpj
					tmp(j) = tmpi
					Array.Copy(tmp, ret, ret.Length)
					Continue Do
				End If
				If tmpScore > baseScore Then
					baseScore = tmpScore
					score = tmpScore
					tmp(i) = tmpj
					tmp(j) = tmpi
					Continue Do
				End If
				Dim te As Double = Math.Pow(Alpha, CDbl(c) / MaxTime)
				Dim ee As Double = Math.Exp(CDbl(tmpScore - baseScore) / te)
				
				XSFT = XSFT Xor (XSFT << 13)
				XSFT = XSFT Xor (XSFT >> 17)
				XSFT = XSFT Xor (XSFT << 5)
				Dim rand As Double = CDbl(XSFT) / UMAX
				If rand < ee Then
					baseScore = tmpScore
					score = tmpScore
					tmp(i) = tmpj
					tmp(j) = tmpi
					Continue Do
				End If
			Loop
			
			' Console.Error.WriteLine("loop:{0} rate:{1}", c, CDbl(c) / MaxTime)
			
			t1 = Environment.TickCount
		Loop While t1 < t0
		
		permute = ret
        
	End Function

End Class