Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System

Public Module Research

    Public Sub Main()
        Try
            Dim N As Integer = CInt(Console.ReadLine())
			Dim K As Integer = CInt(Console.ReadLine())
            Dim constraints(K - 1) As String
            For i As Integer = 0 To UBound(constraints)
                constraints(i) = Console.ReadLine()
            Next i
            
            Dim seed As Integer = CInt(Environment.GetCommandLineArgs()(1))

            Console.Error.WriteLine( _
                "seed: {0:D8}, N: {1,4:D} Data size(K): {2,4:D}" _
                , seed, N, K)
            
            Dim ret(N-1) As Integer
            For i As Integer = 0 To UBound(ret)
                ret(i) = i
            Next i

            Console.WriteLine(ret.Length)
            For Each r As Integer In ret
                Console.WriteLine(r)
            Next r
            Console.Out.Flush()
            
        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module